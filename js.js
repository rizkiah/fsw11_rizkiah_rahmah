OPERATOR DAN LOGIKA
let arrays = [];
let fruits = ["Jeruk", "Semangka"];

arrays = fruits;

console.log(arrays);

let number_one = 10;
let number_two = 20;

console.log(number_one + number_two);
console.log(number_one - number_two);
console.log(number_one * number_two);
console.log(number_one / number_two);
console.log(number_one % number_two);

let firma = 30;
let annisa = 25;
let andi = 32;
let max_age = 30;
let kentangan = true;
let kentingin = false;

if (kentangan == true && kentingin == true) {
  console.log("Bukan kentingin");
}

if (kentingin == false || kentangan == true) {
  console.log("Kentungun");
}

// if (max_age > 30) {
//   console.log("Umur diatas 30");
// }

// if (max_age == 30) {
//   console.log("Umur pas 30");
// }

if (firma == max_age) {
  console.log("Umurnya udah sampe nih");
}

if (annisa > max_age) {
  console.log("Umurnya sudah tua");
}

// if (max_age < 30) {...}

// if (max_age == 30) {...}

// if (max_age >= 30) {...}

// if (max_age <= 30) {...}

// if (max_age != 30) {...}

------------------------------------------
console.log("===========================");
console.log("Hello External JS");

console.info("Result : " + (4 + 8));

/**
 * Definisi variabel
 */
var names = "Kanzoon"; // Inisialisasi dan set default value
names = "Okta"; // Bisa diganti2/Mutable value
const age = 20; // Deklarasi variabel statis/Immutable value
let price = 10000; // Deklarasi variable non statis/Mutable value
let stock = 0;

if(price == 10000) {
  stock = 80;
  names = "Arif Ganteng";
} // Scope
console.log(stock);
console.log(names);

// Tipe data String
let dog = "James"; // String
// let price = 10000; // Number/Integer
let is_handsome = true; // Boolean
let is_empty = [1, 2, 3, 4]; // Array
let person = {"name": "Aku jelek", "country": "Zimbabwe"}; // Object
let sale = 50.80; // Float/Desimal
person.name;
person.country;
let arayObject = [
    {"name": "Aku jelek", "country": "Zimbabwe"},
    {"name": "Aku ganteng", "country": "Zimbabwe"},
    {"name": "Aku setengah", "country": "Zimbabwe"},
    {"name": "Aku gateng sangat", "country": "Zimbabwe"}
]; // Array of object
console.log(arayObject[1].country);
console.log(arayObject[1].name);